<?php
namespace App\Controller;

use App\Entity\Livre;
use App\Entity\Theme;
use App\Entity\PropertySearch;
use App\Repository\LivreRepository;
use App\Form\PropertySearchType;
use PhpParser\Node\Expr\Cast\String_;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
Use Symfony\Component\Routing\Annotation\Route;
class IndexController extends AbstractController
{
 /**
 *@Route("/")
 */
 public function home(Request $request)
 {
   $propertySearch = new PropertySearch();
   $form = $this->createForm(PropertySearchType::class,$propertySearch);
   $form->handleRequest($request);
   //initialement le tableau des articles est vide,
   //c.a.d on affiche les articles que lorsque l'utilisateur
   //clique sur le bouton rechercher
   $livres= [];
  
   if($form->isSubmitted() && $form->isValid()) {
   //on récupère le nom d'article tapé dans le formulaire
   $nom = $propertySearch->getNom();
 if ($nom!="")
 //si on a fourni un nom d'article on affiche tous les articles ayant ce nom
 $livres= $this->getDoctrine()->getRepository(Livre::class)->findByMot($nom);
else
 //si si aucun nom n'est fourni on affiche tous les articles
 $livres= $this->getDoctrine()->getRepository(Livre::class)->findAll();
}
 return $this->render('index.html.twig',[ 'form' =>$form->createView(), 'livres' => $livres]);

 }

 /**
 * @Route("{codelivre}", name="livre_show")
 */

 public function show($codelivre) {
   $livre = $this->getDoctrine()->getRepository(Livre::class)->find($codelivre);
   return $this->render('show.html.twig',array('livre' => $livre));
    }

}