<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Theme
 *
 * @ORM\Table(name="theme")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass=App\Repository\ThemeRepository::class)
 */
class Theme
{
    /**
     * @var int
     *
     * @ORM\Column(name="codeTheme", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codetheme;

    /**
     * @var string
     *
     * @ORM\Column(name="nomTh", type="string", length=15, nullable=false)
     */
    private $nomth;

    public function getCodetheme(): ?int
    {
        return $this->codetheme;
    }

    public function getNomth(): ?string
    {
        return $this->nomth;
    }

    public function setNomth(string $nomth): self
    {
        $this->nomth = $nomth;

        return $this;
    }


}
